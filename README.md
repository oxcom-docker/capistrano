# Capistrano
Source for the [Capistrano](https://github.com/capistrano/capistrano) docker [images](https://hub.docker.com/repository/docker/oxcom/capistrano).

## Local build
```bash
docker build . --rm -t cap-alpine:latest -f ./alpine/Dockerfile
docker build . --rm -t cap-debian:latest -f ./debian/Dockerfile
```

## Run
```bash
docker run -it --rm -v /var/www/oxcom.me/:/var/project cap-debian /bin/bash -c "bundler install && cap -n test deploy"
```
